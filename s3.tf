module "s3_bucket" {
  source                   = "terraform-aws-modules/s3-bucket/aws"
  version                  = "4.1.1"
  bucket                   = format("%s-%s", terraform.workspace, var.s3_bucket_name)
  acl                      = "public-read"
  control_object_ownership = true
  object_ownership         = "ObjectWriter"
  versioning = {
    enabled = true
  }
  website = {
    index_document = "index.html"
  }
  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm = "AES256"
      }
    }
  }
  restrict_public_buckets = false
  ignore_public_acls      = false
  block_public_acls       = false
  block_public_policy     = false
  attach_policy           = true
  force_destroy           = true
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "StaticWebsite",
          "Effect" : "Allow",
          "Principal" : "*",
          "Action" : "s3:GetObject*",
          "Resource" : [
            "arn:aws:s3:::${terraform.workspace}-${var.s3_bucket_name}/*",
            "arn:aws:s3:::${terraform.workspace}-${var.s3_bucket_name}"
          ]
        }
      ]
    }
  )
}
