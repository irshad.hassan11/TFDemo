resource "aws_s3_object" "index" {
  bucket       = module.s3_bucket.s3_bucket_id
  acl          = "public-read"
  key          = "index.html"
  source       = "./index.html"
  etag         = filemd5("./index.html")
  content_type = "text/html"
}
