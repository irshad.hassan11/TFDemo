variable "s3_bucket_name" {
  description = "(Required) Name for unique S3 bucket"
  type        = string
  default     = "mydemo-bucket-tsys"
}