# TFDemo Overview

- The repo is to host static website straight from S3 bucket.

- Have used cloudfront distribution as public read to s3 OAI 

- Assumptions and explanation is below in relevant section.

## Terraform (IAC) consideration

- terraform version 1.6.6 is used

- Demonstrated the use of modules in this case from terraform registry

- used terraform workapce to support multi environment deployment

- In order to make this simple, I have created tfstate bucket to initialise terraform backend manually - bucket name - `platforms-tf-state-tfdemo`

- Tried not to use administrator for pipeline deploy user's api key have used s3:* only

- Demoed use of variables rather then hardcoding which is inline with tflint in variables.tf to make it reusable as well

## Gitlab CI consideration

- In order to run the pipeline need to pass variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for the deploy user

## Endpoint 

- Have used outputs.tf which displays the `cloudfront_distribution_domain_name` at the end of apply stage. That endpoint cat be tested either by curl or directly paste onto browser
 reference job to get output endpoint is - [endpoint_link](https://gitlab.com/irshad.hassan11/TFDemo/-/jobs/6526887814)

- `curl <cloudfront_distribution_domain_name>` should give the output