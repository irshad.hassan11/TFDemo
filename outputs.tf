output "s3_bucket_website_endpoint" {
  description = "The AWS region this bucket resides in."
  value       = module.s3_bucket.s3_bucket_website_endpoint
}

output "cloudfront_distribution_domain_name" {
  description = "The domain name corresponding to the distribution."
  value       = try(aws_cloudfront_distribution.static_s3_distribution.domain_name, "")
}